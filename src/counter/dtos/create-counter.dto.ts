/* eslint-disable prettier/prettier */
import { IsNotEmpty } from 'class-validator';

export class CreateCounterDto {
  @IsNotEmpty()
  readonly ower: string;

  @IsNotEmpty()
  readonly CEP: string;

  @IsNotEmpty()
  readonly street: string;

  @IsNotEmpty()
  readonly number: number;
}
