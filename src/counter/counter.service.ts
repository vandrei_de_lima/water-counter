import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Counter } from './interfaces/counter.interface';
import { Model } from 'mongoose';
import { CreateCounterDto } from './dtos/create-counter.dto';

@Injectable()
export class CounterService {
  constructor(
    @InjectModel('Counter') private readonly counterModel: Model<Counter>,
  ) {}

  async createCounter(counter: CreateCounterDto): Promise<void> {
    const { street } = counter;

    const counterDb = await this.counterModel.findOne({ street }).exec();

    if (counterDb?.number === counter.number)
      throw new BadRequestException(`Endereço já cadastrado`);

    this.create(counter);
  }

  private async create(counter: CreateCounterDto): Promise<Counter> {
    const newCounter = new this.counterModel(counter);

    return newCounter.save();
  }

  async updateCounter(
    _id: string,
    counter: CreateCounterDto | Counter,
  ): Promise<void> {
    const counterDb = await this.counterModel.findOne({ _id }).exec();

    if (!counterDb)
      throw new BadRequestException(`Contador com o id ${_id} não encontrado`);

    await this.counterModel.findOneAndUpdate({ _id }, { $Set: counter }).exec();
  }

  async getCounters(): Promise<Counter[]> {
    return this.counterModel.find().exec();
  }

  async getById(_id: string): Promise<Counter> {
    return this.counterModel.findById(_id);
  }

  async getByMail(mail: string): Promise<Counter> {
    return this.counterModel.findOne({ mail }).exec();
  }

  async deleteById(_id: string): Promise<void> {
    const counterDb = await this.counterModel.findById(_id).exec();

    if (!counterDb)
      throw new BadRequestException(`Counter com id ${_id} não cadastrado`);

    return this.counterModel.remove({ _id }).exec();
  }

  async deleteAll(): Promise<void> {
    return this.counterModel.remove().exec();
  }
}
