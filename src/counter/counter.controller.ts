import {
  Body,
  Controller,
  Param,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CounterService } from './counter.service';
import { CreateCounterDto } from './dtos/create-counter.dto';
import { CustomValidatorPipe } from './../shared/pipes/validator.pipe';
import { Delete, Get } from '@nestjs/common/decorators';

@Controller('api/v1/counter')
export class CounterController {
  constructor(private counterService: CounterService) {}

  @Post('create-counter')
  @UsePipes(ValidationPipe)
  async createOwer(@Body() counter: CreateCounterDto) {
    return this.counterService.createCounter(counter);
  }

  @Post('update-counter/:_id')
  @UsePipes(ValidationPipe)
  async updateCounter(
    @Body() counter: CreateCounterDto,
    @Param('_id', CustomValidatorPipe) _id: string,
  ) {
    return this.counterService.updateCounter(_id, counter);
  }

  @Get()
  async getCounter() {
    return this.counterService.getCounters();
  }

  @Get('by-id/:_id')
  async getById(@Param('_id', CustomValidatorPipe) _id: string) {
    return this.counterService.getById(_id);
  }

  @Get('by-id/:mail')
  async getByMail(@Param('mail', CustomValidatorPipe) mail: string) {
    return this.counterService.getById(mail);
  }

  @Delete('by-id/:_id')
  async deleteByMail(@Param('_id', CustomValidatorPipe) _id: string) {
    return this.counterService.deleteById(_id);
  }

  @Delete()
  async deleteAll() {
    return this.counterService.deleteAll();
  }
}
