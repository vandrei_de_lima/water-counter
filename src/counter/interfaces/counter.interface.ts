/* eslint-disable prettier/prettier */
import { Document } from 'mongoose';

export interface Counter extends Document {
  events: string[];
  CEP: string;
  street: string;
  number: number;
  ower: string;
  lastUpdate: Date;
}
