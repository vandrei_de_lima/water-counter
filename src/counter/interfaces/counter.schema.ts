/* eslint-disable prettier/prettier */
import * as mongoose from 'mongoose';

export const CounterSchema = new mongoose.Schema(
  {
    lastUpdate: { type: Date },
    CEP: { type: String },
    street: { type: String },
    number: { type: Number },
    events: { type: Array },
    // ower: { type: String },
    ower: { type: mongoose.Schema.Types.ObjectId, ref: 'Ower' },
  },
  { timestamps: true, collection: 'counter' },
);
