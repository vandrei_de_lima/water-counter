import { Module } from '@nestjs/common';

import { OwerModule } from './ower/ower.module';
import { CounterModule } from './counter/counter.module';
import { MongooseModule } from '@nestjs/mongoose';

const url =
  'mongodb+srv://vandrei:u7WjtP6fT54cV7fJ@cluster0.osogm6s.mongodb.net/?retryWrites=true&w=majority';

@Module({
  imports: [
    OwerModule,
    CounterModule,
    MongooseModule.forRoot(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
