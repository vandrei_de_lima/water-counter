import {
  Controller,
  Post,
  UsePipes,
  ValidationPipe,
  Body,
  Param,
  Get,
  Delete,
} from '@nestjs/common';
import { CreateOwerDto } from './dtos/create-ower.dto';
import { CustomValidatorPipe } from '../shared/pipes/validator.pipe';
import { OwerService } from 'src/ower/ower.service';

@Controller('api/v1/ower')
export class OwerController {
  constructor(private owerService: OwerService) {}

  @Post('create-ower')
  @UsePipes(ValidationPipe)
  async createOwer(@Body() ower: CreateOwerDto) {
    return this.owerService.createOwer(ower);
  }

  @Post('update-ower/:_id')
  @UsePipes(ValidationPipe)
  async updateOwer(
    @Body() ower: CreateOwerDto,
    @Param('_id', CustomValidatorPipe) _id: string,
  ) {
    return this.owerService.updateOwer(_id, ower);
  }

  @Get()
  async getOwers() {
    return this.owerService.getOwers();
  }

  @Get('by-id/:_id')
  async getById(@Param('_id', CustomValidatorPipe) _id: string) {
    return this.owerService.getById(_id);
  }

  @Get('by-id/:mail')
  async getByMail(@Param('mail', CustomValidatorPipe) mail: string) {
    return this.owerService.getById(mail);
  }

  @Delete('by-id/:_id')
  async deleteByMail(@Param('_id', CustomValidatorPipe) _id: string) {
    return this.owerService.deleteById(_id);
  }

  @Delete()
  async deleteAll() {
    return this.owerService.deleteAll();
  }
}
