/* eslint-disable prettier/prettier */
import { IsNotEmpty } from 'class-validator';

export class CreateOwerDto {
  @IsNotEmpty()
  readonly phone: string;

  @IsNotEmpty()
  readonly mail: string;
}
