/* eslint-disable prettier/prettier */
import * as mongoose from 'mongoose';

export const OwerSchema = new mongoose.Schema(
  {
    phone: { type: String, unique: true },
    mail: { type: String, unique: true },
    name: { type: String },
  },
  { timestamps: true, collection: 'ower' },
);
