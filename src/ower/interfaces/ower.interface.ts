/* eslint-disable prettier/prettier */
import { Document } from 'mongoose';

export interface Ower extends Document {
  phone: string;
  mail: string;
  name: string;
}
