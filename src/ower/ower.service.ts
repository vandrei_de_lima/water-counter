import { BadRequestException, Injectable } from '@nestjs/common';
import { Ower } from './interfaces/ower.interface';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateOwerDto } from './dtos/create-ower.dto';

@Injectable()
export class OwerService {
  constructor(@InjectModel('Ower') private readonly owerModel: Model<Ower>) {}

  async createOwer(ower: CreateOwerDto): Promise<void> {
    const { mail } = ower;

    const owerDb = await this.owerModel.findOne({ mail }).exec();

    if (owerDb)
      throw new BadRequestException(
        `Proprietario com o email ${mail} já cadastrado`,
      );

    this.create(ower);
  }

  private async create(player: CreateOwerDto): Promise<Ower> {
    const newOwer = new this.owerModel(player);

    return newOwer.save();
  }

  async updateOwer(_id: string, ower: CreateOwerDto | Ower): Promise<void> {
    const playerDb = await this.owerModel.findOne({ _id }).exec();

    if (!playerDb)
      throw new BadRequestException(
        `Proprietario com o id ${_id} não encontrado`,
      );

    await this.owerModel.findOneAndUpdate({ _id }, { $set: ower }).exec();
  }

  async getOwers(): Promise<Ower[]> {
    return this.owerModel.find().exec();
  }

  async getById(_id: string): Promise<Ower> {
    return this.owerModel.findOne({ _id }).exec();
  }

  async getByMail(mail: string): Promise<Ower> {
    return this.owerModel.findOne({ mail }).exec();
  }

  async deleteById(_id: string): Promise<void> {
    const playerDb = await this.owerModel.findOne({ _id }).exec();

    if (!playerDb)
      throw new BadRequestException(
        `Proprietario com o id ${_id} não encontrado`,
      );

    return this.owerModel.remove({ _id }).exec();
  }

  async deleteAll(): Promise<void> {
    return this.owerModel.remove().exec();
  }
}
