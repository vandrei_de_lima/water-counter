import { Module } from '@nestjs/common';
import { OwerController } from './ower.controller';
import { OwerService } from './ower.service';
import { MongooseModule } from '@nestjs/mongoose';
import { OwerSchema } from './interfaces/ower.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Ower', schema: OwerSchema }])],
  controllers: [OwerController],
  providers: [OwerService],
})
export class OwerModule {}
